# stucco
A discussion platform.
## Configuration guide
All configuration goes into `config.json`. Here is a sample:
```
{
  "log": {
    "level": "silly"
  },
  "firebase": {
    "credential": <firebase service account credential>,
    "databaseURL": "https://redacted.firebaseio.com"   
  },
  "url": "localhost:3000",
  "assets_dir": "/home/user/stucco/assets"
}
```

- `log.level` is optional.
- `url` is *mandatory*, because otherwise stucco *will* break.
- `assets_dir` is the (preferrably *absolute*) path of the assets directory. If not set, then stucco will use the - - `<location of index.js>/assets` directory. Keep in mind that this would be accessed from *inside `lib`*, so if not using an absolute path, keep this in mind please.
