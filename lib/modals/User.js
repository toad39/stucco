
const winston = require("winston/lib/winston/config");
const crypt = require("../crypt");
const Modal = require("./Modal");

module.exports = class User extends Modal {
    constructor(exists, opts) {
        super("user");
        this.data.username = opts.username;
        if (!exists) {
            const hash = crypt.hash(opts.password);
            this.data.password = hash.hash;
            this.data.salt = hash.salt;

            winston.verbose(`New user modal created: ${this.data}`);
        }
    }
    ins() {
        // `ins` inserts the current modal into the database. See lib/db.js
    }
};
