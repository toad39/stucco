const winston = require("winston");

const ps = module.exports;

ps.mk = () => {
    process.on("SIGTERM", ps.shutdown);
    process.on("SIGINT", ps.shutdown);
    process.on("SIGHUP", ps.restart);
    process.on("uncaughtException", err => {
        winston.error(err.stack);

        ps.shutdown(100);
    });
};

ps.shutdown = async code => {
    winston.info("{ps} stucco shutting down ...");
    try {
        await require("./server").close();
        await require("./db").close();
        winston.info("{ps} stucco shutdown complete.");
        process.exit(code || 0);
    } catch (err) {
        winston.error(err.stack);
        process.exit(code || 50);
    }
};

ps.restart = () => {
    if (process.send) {
        winston.info("{ps} restarting stucco.");

        process.send({
            action: "restart"
        });
    } else {
        winston.error("{ps} Could not shut down stucco!");
        ps.shutdown(2);
    }
};
