"use strict";

const fs = require("fs");
const nconf = require("nconf");
const express = require("express");
const winston = require("winston");
const app = express();
var server;

if (nconf.get("secure")) {
    server = require("https").createServer({
        key: fs.readFileSync(nconf.get("secure:key")),
        cert: fs.readFileSync(nconf.get("scure:cert"))
    }, app);
} else {
    // http library
    server = require("http").createServer(app);
}

module.exports.server = server;
module.exports.app = app;

server.on("error", e => {
    winston.error(e.stack);
    module.exports.close();
});

var conns = {};
server.on("connection", conn => {
    conns[`${conn.remoteAddress}:${conn.remotePort}`] = conn;
    //winston.http(`${conn.remoteAddress}:${conn.remotePort} connected to the server.`);
    conn.on("close", () => {
        delete conns[`${conn.remoteAddress}:${conn.remotePort}`];
    });
});

module.exports.close = then => {
    server.close(then);
    for (var conn in conns) {
        if (conns[conn]) {
            conns[conn].destroy();
        }
    }
    winston.info("{server} stucco web server shut down!");
};

module.exports.mk = async () => {
    winston.info("{server} stucco Web server starting!");
    app.listen(nconf.get("port"), () => {
        winston.info("{server} stucco web server online!");
    });
};
