const winston = require("winston");
const nconf = require("nconf");
const fs = require("fs");
const db = require("db");
const path = require("path");

const assets = module.exports;

const assets_dir = path.resolve(nconf.get("assets_dir") || "../assets");

assets.create = modal => {
    if (modal.type !== "asset") {
        winston.error("{assets} `assets.create` must only take a `asset` modal!");
        return;
    }
    const path = path.join(assets_dir, `${modal.dir}/${modal.id}`);
    const stream = fs.createWriteStream(path);
    stream.on("error", err => winston.error(err));


    stream.write(modal.data, err => {
        if (err) {
            winston.error(err);
        }
    });
    db.db().collection("assets").doc(modal.id).set({
        path: path
    }).catch(winston.error);
};
