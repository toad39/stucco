const crypto = require("crypto");
const crypt = module.exports;

crypt.hash = text => {
    // Returns {salt, hash}
    var hash = "";
    const salt = this.salt();

    crypto.scrypt(text, salt, 128, (err, dk) => {
        hash = dk.toString("hex");
    });

    return {salt, hash};
};

crypt.salt = (length=20) => {
    return crypto.randomBytes(length).toString("hex");
};
