const admin = require("firebase-admin");
const winston = require("winston");
const nconf = require("nconf");
const { shutdown } = require("./ps");
const db = module.exports;

if (nconf.get("db:pass") === "true") {
    const e = () => {
        winston.debug("{db} passed");
    };
    db.db = e;
    db.mk = e;
    db.add = e;
    db.mkDone = e;
    db.close = e;
    
} else {
    db.db = () => {
        if (!db.mkDone()) {
            winston.error("Database is not initialized before using.");
            shutdown(1);
        }
    
        return admin.firestore();
    };
    
    db.mk = () => {
        try {
            admin.initializeApp({
                credential: admin.credential.cert(nconf.get("firebase:credential")),
                databaseURL: nconf.get("firebase:databaseURL")
            });
            winston.info("{db} Database online!");
        } catch (e) {
            winston.error(e.stack);
            shutdown(1);
        }
        return db.db();
    };
    
    db.add = modal => {
        modal.ins(db.db()).then(winston.info).catch(winston.error);
    };
    
    db.mkDone = () => admin.apps.length === 1;
    
    db.close = () => {
        admin.app().delete();
        winston.info("{db} Closed connection!");
    };
}
