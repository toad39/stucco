/**
 * init.ts
 * 
 * Operations for prestart.
 */
"use strict";

const nconf = require("nconf");
const winston = require("winston");
const pkg = require("../package.json");
const { shutdown } = require("./ps");

const init = module.exports;

init.winston = () => {
    if (!winston.format) return;

    var formats = [];
    formats.push(winston.format.colorize());
    formats.push((winston.format(msg => {
        // Custom timestamp
        msg.level = `[${Math.floor(Date.now() / 1000)}] ${msg.level}`;
        return msg;
    }))());
    formats.push(winston.format.splat());
    formats.push(winston.format.simple());
    var fileFormats = [...formats];
    fileFormats.shift();

    winston.configure({
        level: nconf.get("log:level") || "verbose",
        transports: [
            new winston.transports.Console({
                handleExceptions: true,
                format: winston.format.combine.apply(null, formats)
            }),
            new winston.transports.File({
                filename: "./stucco.log",
                format: winston.format.combine.apply(null, fileFormats)
            })
        ]
    });
};

init.info = () => {
    winston.silly("Hello :)");
    winston.info(`stucco v${pkg.version}`);
    winston.info(`Copyright (C) 2020-${(new Date()).getFullYear()} Toad39`);
    winston.info("This software is under the BSD 3-clause license.");
    winston.info("The license should be included in the LICENSE file that came with this software.");
    winston.info("--------------------------------------------------------------------------------");
};

init.post = async () => {
    try {
        require("./db").mk();
        require("./server").mk();
        require("./ps").mk();

        winston.info("{app} stucco online!");
    } catch (e) {
        winston.error("Failed starting stucco!", e.stack);
        shutdown(1);
    }
};
